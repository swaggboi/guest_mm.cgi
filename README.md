# guest_mm.cgi

The source for the SwaggNet Guestbook CGI script
(http://swagg.net/cgi-bin/guest.cgi). Posts guestbook messages to
a Mattermost channel

This script wants to run alongside three dot files:

1. .mmCreds.xml

   The Mattermost credentials to use

1. .name.bans

   List of "name" values to silently ban from posting This means there
   will be feedback saying "thanks" but doesn't actually post unless
   you want to specify a separate channel ID for the messages flagged
   as spam. Regexp is supported

1. .msg.bans

   List of words or phrases banned from message body. Regexp is
   supported

This script relies on CGI.pm, XML::LibXML and WebService::Mattermost
(alongside its many dependencies)

## Credentials

- Base URL

    This will likely be https://%(your server's hostname)/api/v4/

- Username

    Can be the email you registed with in lieu of actual username

- Password

    You can't borrow mine, sorry

- Channel ID

    In the UI: Select the channel you want then click on the channel
    name in upper part of the screen, click "View Info" then look for
    the tiny nearly invisible letters on the bottom of the window you
    just opened

- Spam Channel ID

    This is optional; normally messages flagged as spam will be
    discarded but if you specify and alternate channel ID here then
    those spammy messages will be posted there instead
